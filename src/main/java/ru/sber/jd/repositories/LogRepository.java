package ru.sber.jd.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.sber.jd.entities.LogEntity;

public interface LogRepository extends CrudRepository <LogEntity, Integer> {

}