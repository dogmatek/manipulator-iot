package ru.sber.jd.dto;


import lombok.Data;
import lombok.Setter;

@Setter
@Data
public class ProgramDto {
    private Integer id;
    private String name;
}
