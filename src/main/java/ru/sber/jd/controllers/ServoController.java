package ru.sber.jd.controllers;

import com.pi4j.io.i2c.I2CFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.sber.jd.services.LogService;
import ru.sber.jd.services.ServoService;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/")
public class ServoController {


    private final ServoService servoService;
    private final LogService logService;


    // Запуск программы манипулятора
    @GetMapping("/run/{pid}")
    public String run(@PathVariable Integer pid) throws InterruptedException, IOException, I2CFactory.UnsupportedBusNumberException {
        logService.add("Получен запрос ServoController.run, pid=" + pid);
        return servoService.run(pid);
    }


    // управление сервоприводами манипулятора
    @GetMapping("/rotation/{pin}/{angle}")
    public String rotation(@PathVariable Integer pin, @PathVariable Integer angle) throws Exception {
        logService.add("Получен запрос ServoController.rotation, pin=" + pin + ", rotation=" + angle);
        return servoService.rotation(pin, angle);
    }

}
